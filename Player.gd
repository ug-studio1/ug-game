extends Node2D

var speed = 200.0

func _process(delta):
	var vector = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	).normalized()
	$Sprite.position += (vector * speed * delta)
